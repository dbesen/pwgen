#!/usr/bin/env python3

import secrets

filename = "./google-10000-english/google-10000-english-usa.txt"
num_words_in_password = 4
num_letters_to_append = 2

num_passwords_to_generate = 100

char_alphabet_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
char_alphabet_lower = "abcdefghijklmnopqrstuvwxyz"
char_nums = "1234567890"
char_special = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?"

min_word_length = 4

split_alphabet = " "

append_alphabet = char_alphabet_upper + char_nums + char_special

####

def randomize_case(word):
    case = secrets.choice((1, 2, 3))
    if case == 1:
        return word.upper()
    elif case == 2:
        return word.lower()
    else:
        return word.title()

def has_all(haystack):
    if not has(haystack, char_alphabet_upper):
        return False
    if not has(haystack, char_alphabet_lower):
        return False
    if not has(haystack, char_nums):
        return False
    if not has(haystack, char_special):
        return False
    return True

def has(haystack, needles):
    return any(needle in haystack for needle in needles)

num = 0
while num < num_passwords_to_generate:

    split_char = secrets.choice(split_alphabet)

    words = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if len(line) < min_word_length:
                continue
            words.append(line)

    pw = [randomize_case(secrets.choice(words)) for i in range(num_words_in_password)]

    pw = split_char.join(pw)

    for i in range(num_letters_to_append):
        pw = pw + secrets.choice(append_alphabet)

    if has_all(pw):
        num += 1
        print(pw)
